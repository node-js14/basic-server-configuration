const { response, request } = require('express');

const userGET = (req = request, res = response, next) => {
  // TODO Parametro de segmento (URL/user?page=10&limit=10)
  const query = req.query;

  // TODO asignando query por defecto
  const { page = 1, limit = 1 } = query;
  res.json({
    msg: 'GET API Controller',
    query,
    page,
    limit,
  });
};

const userPOST = (req, res = response, next) => {
  const body = req.body;

  res.json({
    msg: 'POST API Controller',
    body,
  });
};

const userPUT = (req, res = response, next) => {
  // TODO Parametro de segmento (URL/user/1)
  const id = req.params.id;

  res.json({
    msg: 'PUT API Controller',
    id,
  });
};

const userPATCH = (req, res = response, next) => {
  // TODO Parametro de segmento (URL/user/1)
  const id = req.params.id;

  res.json({
    msg: 'PATCH API Controller',
    id,
  });
};

const userDELETE = (req, res = response, next) => {
  const id = req.params.id;

  res.json({
    msg: 'DELETE API Controller',
    id,
  });
};

module.exports = {
  userGET,
  userPOST,
  userPUT,
  userPATCH,
  userDELETE,
};
