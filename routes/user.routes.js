const express = require('express');
const {
  userGET,
  userPOST,
  userPUT,
  userPATCH,
  userDELETE,
} = require('../controllers/user.controllers');

const router = express.Router();

router.get('/', userGET);

router.post('/', userPOST);

router.put('/:id', userPUT);

router.patch('/:id', userPATCH);

router.delete('/', userDELETE);

module.exports = router;
