const express = require('express');
const cors = require('cors');

class Server {
  constructor() {
    this.app = express();
    this.port = process.env.PORT;

    //  TODO API USER
    this.userRoutesPath = '/api/user';

    //  TODO Middlewares
    this.middleware();

    //  TODO Rutas
    this.routes();
  }
  middleware() {
    //  TODO CORS
    this.app.use(cors());

    // TODO lectura y parseo de codigo
    this.app.use(express.json());

    //  TODO Directorio publico
    this.app.use(express.static('public'));
  }

  routes() {
    this.app.use(this.userRoutesPath, require('../routes/user.routes'));
  }

  listen() {
    this.app.listen(this.port, () => {
      console.log(`http://localhost:${this.port}`);
    });
  }
}

module.exports = Server;
